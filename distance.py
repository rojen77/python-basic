import math

# def dis(x1,x2,y1,y2):
#     distance = math.sqrt((x1-x2)**2 + (y1-y2)**2)
#     return distance
# print('distance =',dis(2,3,4,1))

class Point:
    x = None
    y = None
    def quadrant(self):

        if self.x==0 or self.y == 0:
            return None
        if self.x>0 and self.y>0:
            return 1
        if self.x<0 and self.y>0:
            return 2
        if self.x<0 and self.y<0:
            return 3
        if self.x > 0 and self.y <0:
            return 4

a = Point()
a.x = 5
a.y = 3

b = Point()
b.x = 7
b.y = 10

c = Point()
c.x = 2
c.y = -3

d = Point()
d.x = -2
d.y = 1

e = Point()
e.x = 4
e.y = 0

f = Point()
f.x = -4
f.y = -1

print('a is in', a.quadrant())
print('b is in', b.quadrant())
print('c is in', c.quadrant())
print('d is in', d.quadrant())
print('e is in', e.quadrant())
print('f is in', f.quadrant())





# distance = math.sqrt((a.x-b.x)**2 + (a.y-b.y)**2)
# print(distance)
# print(type(a),(b))