import math
def calculate_simple(p,t,r):
    simple = (p*t*r) / 100
    return simple

print('when p = 10000 , t = 2yrs, r = 8% => simple=', calculate_simple(10000,2,8))

#calculate area of circle

def calculate_area(r):
    area = math.pi * r ** 2
    return area
print('when r = 5 , area =', calculate_area(5))


#calculate voltage

def calculate_vol(i,r):
    vol = i*r
    return vol
print('When i=5 ,r=2 ,Voltage =', calculate_vol(5,2))


#calculate distance between points

def calculate_dis(x1,x2,y1,y2):
    dis = math.sqrt((x1-x2)**2 + (y1-y2)**2)
    return dis
print('When x1=2, x2=3, y1=3, y2=1, distance=', calculate_dis(2,3,3,1))

#Convert Celcius to Fahernheit

def calculate_far(c):
    far = c*(9/5)+32
    return far
print('When c=20, far=', calculate_far(20))
print('Far =',int(calculate_far(20)))

#day_name

def day_name(num):
    if num == 1 :
        return 'Sunday'
    elif num == 2 :
        return 'Monday'
    elif num == 3 :
        return 'Tuesday'
    elif num == 4 :
        return 'saturday'
    elif num >= 5 :
        return 'invalid number'
print(day_name(2))

#calculate_area_of_triangle

def calculate_tri(a,b,c):
    h = max(a,b,c)
    hsq = h**2
    result = (a**2 + b**2 + c**2) / 2
    if hsq == result:
        print("True")
    else:
        print("False")

calculate_tri(3,4,5)

def calculate_am(a,b):
    am = (a+b)/2
    return am

print('when a=2, b=4, AM=', calculate_am(2,4))
















