#Inheritance
class Employee:
    def __init__(self,name,address,salary):
        self.name = name
        self.address = address
        self.salary = salary

    def calculate_tax(self):
        return 0.01 * self.salary


class Teacher(Employee):
    def __init__(self, name, address, full_time, salary, subject):
        self.full_time = full_time
        self.subject = subject
        super().__init__(name,address,salary)

    def take_class(self):
        print('class taken')


teacher = Teacher('Umesh', 'Kritipur', True, 30000, 'Physics')
print(teacher.calculate_tax())
print(teacher.take_class())

class Helper(Employee):
    def __init__(self, name, address, salary):
        super().__init__(name,address,salary)

    def clean_desk(self):
        print('desk cleaned')

    def manage_parking(self):
        print('parking managed')


helper = Helper('Ram', 'Itahari', 20000)
print(helper.calculate_tax())


class Manager(Employee):
    def __init__(self, name, address, salary, department):
        super().__init__(name,address,salary)
        self.department = department

    def assign_task(self):
        print('Task Assigned')

manager = Manager('Shyam', 'Dharan', 50000, 'IT')
print(manager.calculate_tax())
