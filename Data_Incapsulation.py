class Customer:
    def __init__(self, name, balance, address, account_type):
        self.name = name
        self.__balance = balance
        self.address = address
        self.account_type = account_type

    def deposit(self, amount):
        self.__balance = self.__balance + amount

    def withdraw(self, amount):
        if self.__balance >= amount:
            self.__balance = self.__balance - amount
        else:
            print('Insufficient Balance')

    def print_balance(self):
        print(self.__balance)


john = Customer('John', 10000, 'Koteshwor', 'Saving')
john.deposit(10000)
john.print_balance()

# print(john.__balance)
# john.deposit(10000)
# print(john.__balance)
# join.balance = 0
#
# print(john.__balance)
