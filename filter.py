# new_numbers = [2,3,4,5,6,7,8,9]
#
# check_even_odd = lambda n:n %2 == 0
# filtered_numbers = filter(check_even_odd,new_numbers)
# print(list(filtered_numbers))

sample_list = [2,4,5,6,10,13,15,50,42]
divisible_by_5 = lambda n:n%5 == 0
multiple_of_5 = filter(divisible_by_5,sample_list)
print(list(multiple_of_5))

names = ['Ram Pd Sharma', 'Soman Bd Yadav', 'Roshan K. Rijal', 'Hari Bhusal', 'Umesh Bhatta']
name_without_middle_name = filter(lambda name: len(name.split(' ')) ==2, names)
print(list(name_without_middle_name))
name_with_middle_name = filter(lambda name: len(name.split(' ')) == 3,names)
print(list(name_with_middle_name))


lists = [2,3,5,6,9,12,15,16]
divisible_by_3 = lambda n:n%3 == 0
multiple_of_3 = filter(divisible_by_3,lists)
print(list(multiple_of_3))

place_name = ['Kathmandu,NP', 'Lumbini,NP', 'Goa,IN', 'Texas,USA']
find_place = filter(lambda place : place.split(',')[-1] == 'NP' , place_name)
find_place_1 = filter(lambda place : place.split(',')[-1] in ['NP','IN'] , place_name)
print(list(find_place))
print(list(find_place_1))

sample_name = ['Reyansh Kaustuvh Raj Shrestha', 'Umesh Bhatta', 'Birendra Bir Bikram Shah']
find_long_name = filter(lambda name:len(name) >= 20,sample_name )
print(list(find_long_name))

new_name = ['Abay', 'Umesh', 'Avinash', 'Sahil']
find_name_with_a = filter(lambda name:name[0] == 'A',new_name)
print(list(find_name_with_a))

a = [-1,0,3,4,-7,-3,6,9]
find_positive_number = filter(lambda num: num >= 0,a)
print(list(find_positive_number))

import math

sample_number = [4,8,12,16,20,25,30,64]
perfect_square = filter(lambda n:math.sqrt(n) == int(math.sqrt(n)),sample_number )
print(list(perfect_square))

