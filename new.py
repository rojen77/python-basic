class Time:
    def __init__(self, hour,min,sec):
        self.hour = hour
        self.min = min
        self.sec = sec

    def __str__(self):
        return '%d hr:%d min:%d sec' % (self.hour, self.min, self.sec)

    def __add__(self, other):
        sec = self.sec + other.sec
        if sec >= 60:
            minute = self.min + other.min +1
            sec = sec - 60
        else:
            minute = self.min + other.min


        minute = self.min + other.min
        if minute >= 60:
            hr = self.hour + other.hour +1
            minute = minute-60
        else:
            hr = self.hour + other.hour


        hr = self.hour + other.hour


        return Time(hr, minute, sec)
a = Time(10,35,10)
b = Time(2,30,5)
# print(a.__str__())
# print(b.__str__())
c = a+b
print(c)