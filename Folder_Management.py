import os

BASE_DIR = r'C:\Users\Lenovo\Desktop\Random'  # r is raw string( raw strings means no any logic)

file_types = {
    'photos': ['jpg','JPG', 'png', 'jpeg', 'gif', 'tif'],
    'documents': ['pdf', 'xlsx', 'csv'],
    'movies': ['mp4', 'MP4'],
    'text': ['txt'],
    'python': ['pyc', 'py']
}
# for creating for folder
for file_type in file_types:
    full_path = os.path.join(BASE_DIR, file_type)
    if not os.path.exists(full_path):
        os.mkdir(full_path)  # mkdir= Make directory
# print(os.listdir(BASE_DIR))     #os.listdir shows the names of files containing in the folder

# logic for moving files to folder
for file in os.listdir(BASE_DIR):
    extension = file.split('.')[-1]
    for key in file_types:
        # print(file, key, file_types[key])
        if extension in file_types[key]:
            print(extension, 'is a', key)
            path_src = os.path.join(BASE_DIR, file)
            path_dest = os.path.join(BASE_DIR, key, file)
            # print(path_src, path_dest)
            os.rename(path_src, path_dest)



