# #Three operators in file read write and append
#
# sample_file = open('hello.txt', 'w')
# sample_file.write('Hello world, It will be in File')
# sample_file.close()
#
# with open('next_hello.txt' , 'w')as sample_file_1:
#     sample_file_1.write('Hello World, It will be in file.')
#
# print('Hello')
#
# with open('sample_file.csv', 'r') as student_file:
#     for line in student_file:
#         print(line)
#
#
# sample_files = open('namaste.txt', 'w')
# sample_files.write('Namaste Napal, Happy New Year')
# sample_files.close()

with open('namaste.txt','w')as sample_file_2:
    sample_file_2.write('Namaste Nepal,')

