import math
class Rectangle:
    length = None
    breadth = None

    def area(self):
        return self.length * self.breadth

    def perimeter(self):
        return 2*(self.length+self.breadth)

    def diagonal(self):
        return math.sqrt(self.length**2 + self.breadth**2)

    def square(self):
        if self.length == self.breadth:
            return 'square'
        else:
            return 'not square'

sample_rect = Rectangle()

sample_rect.length = 3
sample_rect.breadth = 4

print(sample_rect.area())
print(sample_rect.perimeter())
print(sample_rect.diagonal())
print(sample_rect.square())

class Triangle:
    length = None
    breadth = None
    height = None
    s = None

    def area(self):
        s = (self.length+self.breadth+self.height)/2
        return math.sqrt(s*(s-self.length)*(s-self.breadth)*(s-self.height))

sample_tri = Triangle()
sample_tri.length = 2
sample_tri.breadth = 3
sample_tri.height = 4

print(sample_tri.area())