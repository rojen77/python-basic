#for loop

#syntax_1
for i in range(10):
    print(i)

#syntax_2
for j in range(1,15):
    print (j)

#syntax_3
for k in range (10,20,2):
    print(k)

#syntaz_4
for a in range (1,101):
    print(a)

#syntax_5
for b in range (2,102,2):
    print('Even Number =', b)

#syntax_6
for c in range (1,101,2):
    print('Odd number =',c)

#syntax_7
for d in range (5,55,5):
    print('Table ',d)

for i in range(1,11):
    print('6 x',i,'=',i*6)

s = 0
for i in range (1,101):
    s=s+i
print(s)

fact = 1
for i in range (1,6):
    fact=fact*i
print(fact)

fruits = ["apple", "banana", "cherry"]
for x in fruits:
  print(x)

