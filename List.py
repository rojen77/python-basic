Sample_list = ['Ram', 'Shyam', 'Hari','John', 'John']
print(len(Sample_list))

for element in Sample_list:
    print(element)

print(Sample_list[2])

print(Sample_list[4])

print(Sample_list[0:3:1])
print(Sample_list[0:5:3])
print(Sample_list[::-1])

del Sample_list[4]
print(Sample_list)

Sample_list.append('Umesh')
print(Sample_list)

Sample_list [2]= "BIT"
print(Sample_list)

print(Sample_list.count('Ram'))
print(Sample_list.count('John'))

Sample_list.insert(3,'JAM')
print(Sample_list)

print(Sample_list.index('JAM'))

Last_name=Sample_list.pop()
print(Sample_list)

first_name = Sample_list.pop(1)
print(Sample_list)

