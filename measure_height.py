class Height:
    def __init__(self,foot,inch):
        self.foot = foot
        self.inch = inch

    def __str__(self):
        return ('%d.%d') % (self.foot,self.inch)

    def __sub__(self, other):
        inch =  self.inch + other.inch
        if inch >= 12:
            foot = self.foot + other.foot + 1
            inch = inch-60
        else:
            foot = self.foot + other.foot

        foot = self.foot + other.foot
        return Height(foot,inch)

father = Height(5,10)
son = Height(5,11)

print(father+son)
